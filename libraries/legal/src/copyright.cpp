#include <mfn/legal/copyright.hpp>

#include <cpptools/logger.hpp>

namespace mfn { namespace legal {

  void copyright::display_notice()
  {
    cpptools_ilog
      << "MFN  Copyright (C) 2021  Mateusz Tyszczak" << cpptools::fnl
      << "This program comes with ABSOLUTELY NO WARRANTY; This is free" << cpptools::fnl
      << "software, and you are welcome to redistribute it under certain conditions"
      << cpptools::endl;
  }

}} // namespace mfn::legal
