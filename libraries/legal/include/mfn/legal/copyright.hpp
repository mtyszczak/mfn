#pragma once

namespace mfn { namespace legal {

  class copyright {
  public:
    static void display_notice();
  };

}} // namespace mfn::legal
