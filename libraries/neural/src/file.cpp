#include <neural/file.hpp>

#include <array>
#include <cstring>

namespace neural {

file::file( const std::string& filename, byte_t cols )
  : std::fstream( filename, file_open_flags )
{
  write( static_cast< const uint8_t* >( mfn_magic_bytes ), sizeof( mfn_magic_bytes ) / sizeof( byte_t ) );
  set_special( cols );
}

void file::open( const std::string& filename )
{
  if( is_open() )
    close();
  std::fstream::open( filename, file_open_flags );
}

void file::set_special( byte_t special_byte, bool restore_pos )
{
  pos_type before_pos = tellg();
  seekg( sizeof( mfn_magic_bytes ) / sizeof( byte_t ), beg );
  write( static_cast< const uint8_t* >( &special_byte ), sizeof( special_byte ) );
  if( restore_pos )
    seekg( before_pos, beg ); // restore old pos
}

void file::write( const uint8_t* data, size_t length )
{
  std::fstream::write( reinterpret_cast< const char* >( data ), length );
}
void file::readsome( uint8_t* data, size_t length )
{
  std::fstream::readsome( reinterpret_cast< char* >( data ), length );
}

void file::fall( bool ignore_magic_bytes )
{
  if( ignore_magic_bytes )
    seekg( 0, beg );
  else
    seekg( sizeof( mfn_magic_bytes ) / sizeof( byte_t ) + sizeof( byte_t ), beg );
}

bool file::has_magic_bytes()
{
  pos_type before_pos = tellg();
  fall( true );
  std::array< byte_t, sizeof( mfn_magic_bytes ) / sizeof( byte_t ) > load_buf;
  read( reinterpret_cast< char* >( load_buf.data() ), load_buf.size() );
  seekg( before_pos, beg ); // restore old pos
  return !static_cast< bool >( memcmp( mfn_magic_bytes, load_buf.data(), load_buf.size() ) );
}

file::byte_t file::get_special()
{
  pos_type before_pos = tellg();
  seekg( sizeof( mfn_magic_bytes ) / sizeof( byte_t ), beg );
  byte_t special = 0;
  read( reinterpret_cast< char* >( &special ), sizeof( special ) );
  seekg( before_pos, beg ); // restore old pos
  return special;
}

bool file::next_eof()
{
  return peek() == traits_type::eof();
}

} // namespace neural
