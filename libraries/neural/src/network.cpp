#include <neural/network.hpp>

#include <eigen3/Eigen/Core> // For Index

#include <cassert>
#include <functional> // For cref and function (lambda)
#include <memory>     // For unique_ptr
#include <vector>

namespace neural {
network::activation_function_t network::propagate_forward_activation_function = []( scalar x ) -> scalar { return tanhf( x ); };
network::activation_function_t network::update_weights_activation_function    = []( scalar x ) -> scalar { return 1 - tanhf( x ) * tanhf( x ); };

network::network( const std::vector< uint32_t >& topology, scalar learning_rate )
  : learning_rate( learning_rate )
{
  neuron_layers.reserve( topology.size() );
  cache_layers.reserve( topology.size() );
  deltas.reserve( topology.size() );
  weights.reserve( topology.size() - 1 );

  for( auto it = topology.begin(); it != topology.end(); ++it )
  {
    neuron_layers.emplace_back( std::make_unique< learn_type >( *it + /* bias: */ 1 ) );

    cache_layers.emplace_back( std::make_unique< learn_type >( neuron_layers.size() ) );
    deltas.emplace_back( std::make_unique< learn_type >( neuron_layers.size() ) );

    if( it != topology.end() - 1 )
    {
      neuron_layers.back()->coeffRef( *it ) = 1.0;
      cache_layers.back()->coeffRef( *it )  = 1.0;
    }

    if( it > topology.begin() )
    {
      if( it != topology.end() - 1 )
      {
        weights.emplace_back( std::make_unique< matrix >( *( it - 1 ) + 1, *it + 1 ) );
        weights.back()->setRandom();
        weights.back()->col( *it ).setZero();
        weights.back()->coeffRef( *( it - 1 ), *it ) = 1.0;
      }
      else
      {
        weights.emplace_back( std::make_unique< matrix >( *( it - 1 ) + 1, *it ) );
        weights.back()->setRandom();
      }
    }
  }
}

void network::propagate_forward( const learn_type& input,
  const activation_function_t& activation_function ) const
{
  // block: startRow, startCol, blockRows, blockCols
  neuron_layers.front()->block( 0, 0, 1, neuron_layers.front()->size() - 1 ) = input;

  for( size_t i = 1; i < neuron_layers.size(); ++i )
    *neuron_layers.at( i ) = ( *neuron_layers.at( i - 1 ) ) * ( *weights.at( i - 1 ) );

  for( size_t i = 1; i < neuron_layers.size() - 1; ++i )
    neuron_layers[i]->block( 0, 0, 1, neuron_layers[i]->size() ).unaryExpr( std::cref( activation_function ) );
}

void network::propagate_backward( const learn_type& output ) const
{
  calc_errors( output );
  update_weights();
}

void network::calc_errors( const learn_type& output ) const
{
  // calculate the errors made by neurons of last layer
  *deltas.back() = output - ( *neuron_layers.back() );

  // error calculation of hidden layers is different
  for( size_t i = neuron_layers.size() - 2; i > 0; --i )
    *deltas[i] = ( *deltas[i + 1] ) * ( weights[i]->transpose() );
}

void network::update_weights( const activation_function_t& activation_function_derivative ) const
{
  for( size_t i = 0; i < neuron_layers.size() - 1; ++i )
    if( i != neuron_layers.size() - 2 )
      for( Eigen::Index c = 0; c < weights[i]->cols() - 1; ++c )
        for( Eigen::Index r = 0; r < weights[i]->rows(); ++r )
          weights[i]->coeffRef( r, c ) += learning_rate
                                        * deltas[i + 1]->coeffRef( c )
                                        * activation_function_derivative( cache_layers[i + 1]->coeffRef( c ) )
                                        * neuron_layers[i]->coeffRef( r );

    else
      for( Eigen::Index c = 0; c < weights[i]->cols(); ++c )
        for( Eigen::Index r = 0; r < weights[i]->rows(); ++r )
          weights[i]->coeffRef( r, c ) += learning_rate
                                        * deltas[i + 1]->coeffRef( c )
                                        * activation_function_derivative( cache_layers[i + 1]->coeffRef( c ) )
                                        * neuron_layers[i]->coeffRef( r );
}

const network::learn_type& network::train( const learn_type& in, const learn_type& out ) const
{
  propagate_forward( in );
  propagate_backward( out );
  return *neuron_layers.back();
}

scalar network::mse() const
{
  return std::sqrt( ( *deltas.back() ).dot( ( *deltas.back() ) ) / deltas.back()->size() );
}

const network::learn_type& network::operator()( const learn_type& in ) const
{
  propagate_forward( in );
  return *neuron_layers.back();
}

} // namespace neural
