#pragma once

#include <eigen3/Eigen/Eigen>

#include <cassert>
#include <functional>  // For function (lambda)
#include <memory>      // For unique_ptr
#include <type_traits> // For is_same, enable_if
#include <vector>

#include <cpptools/concepts.hpp>
#include <cpptools/streamable.hpp>

#include <neural/eigen_impl.hpp>

namespace neural {
using scalar     = float; // TODO: Fix this so user can switch between scalar types (maybe stop using eigen)
using matrix     = Eigen::Matrix< scalar, -1, -1 >;
using row_vector = Eigen::Matrix< scalar, 1, -1 >;

class network {
public:
  using learn_type            = row_vector;
  using activation_function_t = std::function< scalar( scalar ) >;

  static activation_function_t propagate_forward_activation_function, update_weights_activation_function;

  /// @brief Constructs new neural network with given topology (number of neurons in each layer) and learning rate
  network( const std::vector< uint32_t >& topology, scalar learning_rate = scalar( 0.005 ) );

  void propagate_forward( const learn_type& input,
    const activation_function_t& activation_function = propagate_forward_activation_function ) const;

  void propagate_backward( const learn_type& output ) const;

  void calc_errors( const learn_type& output ) const;

  void update_weights( const activation_function_t& activation_function_derivative
                       = update_weights_activation_function ) const;

  /// @brief fast-trains neural network with given input data and expected output data. Returns the produced output
  const learn_type& train( const learn_type& in, const learn_type& out ) const;
  /// @brief Returns current Mean squared error
  scalar mse() const;

  /// @brief Returns produced output for given input data
  const learn_type& operator()( const learn_type& in ) const;

  template< cpptools::OutputStreamable StreamType >
  void dump( StreamType& s ) const
  {
    const auto pack = [&]( const auto& what ) {
      cpptools::pack( s, static_cast< size_t >( what.size() ) );
      for( const auto& l: what )
        cpptools::pack( s, *l );
    };
    pack( neuron_layers );
    pack( cache_layers );
    pack( deltas );
    pack( weights );
    cpptools::pack( s, learning_rate );
  }

  template< cpptools::InputStreamable StreamType >
  void restore( StreamType& s )
  {
    size_t size          = 0;
    const auto unpack_to = [&]( auto& to ) {
      cpptools::unpack( s, size );
      to.resize( size );
      for( auto& l: to )
      {
        learn_type lt_val;
        cpptools::unpack( s, lt_val );
        l = std::make_unique< learn_type >( lt_val );
      }
    };
    unpack_to( neuron_layers );
    unpack_to( cache_layers );
    unpack_to( deltas );
    cpptools::unpack( s, size );
    weights.resize( size );
    for( auto& l: weights )
    {
      matrix lt_val;
      cpptools::unpack( s, lt_val );
      l = std::make_unique< matrix >( lt_val );
    }
    cpptools::unpack( s, learning_rate );
  }

private:
  /// stores the different layers of out network
  std::vector< std::unique_ptr< learn_type > > neuron_layers;
  /// stores the unactivated (activation fn not yet applied) values of layers
  std::vector< std::unique_ptr< learn_type > > cache_layers;
  /// stores the error contribution of each neurons
  std::vector< std::unique_ptr< learn_type > > deltas;
  /// stores the connection weights
  std::vector< std::unique_ptr< matrix > > weights;

  scalar learning_rate;
};

} // namespace neural

namespace cpptools {

template< OutputStreamable StreamType >
inline void pack( StreamType& s, const neural::network& data )
{
  data.dump( s );
}

template< InputStreamable StreamType >
inline void unpack( StreamType& s, neural::network& data )
{
  data.restore( s );
}

} // namespace cpptools
