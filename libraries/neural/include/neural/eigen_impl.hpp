#pragma once

#include <eigen3/Eigen/Eigen>

#include <cpptools/streamable.hpp>

namespace cpptools {

template< OutputStreamable StreamType, typename S, int C, int R >
inline void pack( StreamType& s, const Eigen::Matrix< S, C, R >& data )
{
  pack( s, static_cast< size_t >( data.cols() ) );
  pack( s, static_cast< size_t >( data.rows() ) );
  for( Eigen::Index c = 0; c < data.cols(); ++c )
    for( Eigen::Index r = 0; r < data.rows(); ++r )
      pack( s, data.coeffRef( r, c ) );
}

template< OutputStreamable StreamType, typename S, int C, int R >
inline void unpack( StreamType& s, Eigen::Matrix< S, C, R >& data )
{
  size_t r = 0, c = 0;
  unpack( s, c );
  unpack( s, r );
  data.resize( r, c );
  for( Eigen::Index c = 0; c < data.cols(); ++c )
    for( Eigen::Index r = 0; r < data.rows(); ++r )
      unpack( s, data.coeffRef( r, c ) );
}

} // namespace cpptools
