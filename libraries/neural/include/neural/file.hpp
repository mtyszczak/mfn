#pragma once

#include <fstream>
#include <vector>

#include <cpptools/streamable.hpp>

namespace neural {

/**
 * @brief Appends MFN magic bytes at the beginning of the file in the constructor
 */
class file : public std::fstream, public cpptools::raw::istream, public cpptools::raw::ostream {
public:
  typedef uint8_t byte_t;

  static constexpr std::ios_base::openmode file_open_flags = std::ios_base::trunc | std::ios_base::out | std::ios_base::in | std::ios_base::binary;

  static constexpr const byte_t mfn_magic_bytes[] = { 'M', 'F', 'N' };

  file() = default;
  file( const std::string& filename, byte_t special_byte = 0 );

  /// @brief closes previously opened file (if any) and opens given file
  void open( const std::string& filename );

  /// @brief sets special character (that will be changed in the magic bytes of the file)
  void set_special( byte_t special_byte, bool restore_pos = false );

  virtual void write( const uint8_t* data, size_t length ) override;
  virtual void readsome( uint8_t* data, size_t length ) override;

  /**
   * @brief seekg to the beginning of the file (either including magic_bytes or not)
   * @note if ignore_magic_bytes is set to true fall will seekg to 0 offset, if not (by default) then to 4 bytes offset
   */
  void fall( bool ignore_magic_bytes = false );

  /// @brief returns either true or false if next character is the end of file
  bool has_magic_bytes();

  byte_t get_special();

  /// @brief returns either true or false if next character is the end of file
  bool next_eof();
};

} // namespace neural
