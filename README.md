# MFN

Multifunctional neural network

## Building

### Compile-Time Options (cmake)

#### MFN_FAIL_ON_WARNINGS=[ON/OFF] (OFF)

Treat compiler warnings as errors

#### MFN_STATIC_BUILD=[ON/OFF] (OFF)

Builds MFN as a statically linked program instead of as a dynamically linked one

### Building on Ubuntu 22.04

```sh
# install dependencies (opencv and boost)
sudo apt update

sudo apt install -y \
    cmake \
    g++ \
    git \
    make \
    libeigen3-dev # linear algebra

git clone https://gitlab.com/mtyszczak/mfn.git
cd mfn
git checkout main
git submodule update --init --recursive --progress

mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j$(nproc) all

# optional
make install  # defaults to /usr/local
```

This shall generate the binary executable in the build directory that can be run to verify that the make file indeed compiles the project

## Linter

All of the code belonging to the mfn codebase is formatted using this [`.clang-format`](https://gitlab.com/mtyszczak/clang-format-file/-/raw/main/.clang-format) file from [this repository](https://gitlab.com/mtyszczak/clang-format-file)

## License

Distributed under the GNU GENERAL PUBLIC LICENSE Version 3
See [LICENSE.md](LICENSE.md)
