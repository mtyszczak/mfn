cmake_minimum_required( VERSION 3.20 )
project( MFN VERSION 0.1.0 DESCRIPTION "Multifunctional neural network" LANGUAGES CXX )

set( CMAKE_CXX_STANDARD 20 )
set( CMAKE_CXX_STANDARD_REQUIRED OFF )
set( CMAKE_CXX_EXTENSIONS OFF )

option( MFN_FAIL_ON_WARNINGS "Treat compiler warnings as errors" OFF )
option( MFN_STATIC_BUILD "Builds MFN as a statically linked program instead of as a dynamically linked one" OFF )

set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Wpedantic" )
if( MFN_FAIL_ON_WARNINGS )
  set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Werror" )
endif()

add_subdirectory( libraries )
add_subdirectory( programs )
