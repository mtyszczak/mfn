
#include <neural/file.hpp>
#include <neural/network.hpp>

#include <cpptools/exception.hpp>
#include <cpptools/streamable.hpp>

#include <mfn/legal/copyright.hpp>

#include <cstdlib>
#include <ctime>
#include <deque>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <memory>
#include <vector>

namespace {
unsigned learn_data_count = 0;

neural::file in_file{ "input_data.mfn" };
neural::file out_file{ "output_data.mfn" };
neural::file net_file{ "network.mfn" };

inline void gen_data( neural::file& in, neural::file& out );
inline void read_data( neural::file& _file, std::vector< std::unique_ptr< neural::row_vector > >& data );
} // namespace

int main( int argc, char** argv )
{
  try
  {
    mfn::legal::copyright::display_notice();

    CPPTOOLS_ASSERT( argc == 2, "Usage: random_number <learn_data_count>" );
    learn_data_count = std::atoi( argv[1] );

    std::vector< uint32_t > neurons = { 2, 3, 5, 3, 1 };

    neural::network n( neurons );
    if( std::filesystem::exists( "neural.mfn" ) )
      cpptools::unpack( net_file, n );

    std::vector< std::unique_ptr< neural::row_vector > > in_dat, out_dat;

    in_dat.reserve( learn_data_count );
    out_dat.reserve( learn_data_count );

    gen_data( in_file, out_file );
    read_data( in_file, in_dat );
    read_data( out_file, out_dat );

    CPPTOOLS_ASSERT( in_dat.size() == out_dat.size(), "Data size mismatch" );

    // Prevent default scientific notation display
    cpptools_dlog << std::fixed;

    // Train the network using generated data
    for( size_t i = 0; i < in_dat.size(); ++i )
    {
      const auto& in      = *in_dat.at( i );
      const auto& out     = *out_dat.at( i );
      const auto& trained = n.train( in, out );

      cpptools_dlog << "Iteration #" << i + 1 << cpptools::fnl
                    << "  Input to neural network is: " << in << cpptools::fnl
                    << "  Expected output is:         " << out << cpptools::fnl
                    << "  Output produced is:         " << trained << cpptools::fnl
                    << "  MSE:                        " << n.mse() << cpptools::endl;
    }

    net_file.fall();
    std::filesystem::resize_file(
      "network.mfn", sizeof( neural::file::mfn_magic_bytes ) / sizeof( neural::file::byte_t ) + sizeof( neural::file::byte_t ) );
    cpptools::pack( net_file, n );

    return 0;
  }
  CPPTOOLS_CATCH_LOG();

  return -1;
}

namespace {

inline void gen_data( neural::file& in, neural::file& out )
{
  srand( time( NULL ) );

  in.fall( true );
  out.fall( true );

  in.set_special( 2 );  // Two columns
  out.set_special( 1 ); // One column

  for( unsigned r = 0; r < learn_data_count; ++r )
  {
    neural::scalar x = rand() / neural::scalar( RAND_MAX );
    neural::scalar y = rand() / neural::scalar( RAND_MAX );
    in << x << y;
    out << 2 * x + y * 2;
  }
}

inline void read_data( neural::file& _file, std::vector< std::unique_ptr< neural::row_vector > >& data )
{
  CPPTOOLS_ASSERT( _file.has_magic_bytes(), "Magic bytes mismatch" );

  neural::file::byte_t cols = _file.get_special();

  _file.fall();

  while( !_file.next_eof() )
  {
    data.emplace_back( std::make_unique< neural::row_vector >( 1, cols ) );
    _file.read( reinterpret_cast< char* >( data.back()->data() ), sizeof( neural::scalar ) * cols );
  }
}
} // namespace