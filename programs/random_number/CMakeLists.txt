add_executable( random_number main.cpp )

if( MFN_STATIC_BUILD )
  target_link_libraries( random_number PRIVATE
    "-static-libstdc++ -static-libgcc"
    neural cpptools mfn_legal
  )
else()
  target_link_libraries( random_number PRIVATE
    neural cpptools mfn_legal
  )
endif()

install( TARGETS
  random_number

  RUNTIME DESTINATION bin
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib
)
